public class Refrigerator
{
	private int warranty;
	private String brand;
	private int lowestTemp;	
	private int currentTemp;
	
	public Refrigerator(int warranty, String brand, int lowestTemp, int currentTemp)
	{
		this.warranty = warranty;
		this.brand = brand;
		this.lowestTemp = lowestTemp;
		this.currentTemp = currentTemp;
	}
	
	public void setWarranty(int newWarranty)
	{
		this.warranty = newWarranty;
	}
	public void setBrand(String newBrand)
	{
		this.brand = newBrand;
	}
	public void setLowestTemp(int newLowestTemp)
	{
		this.lowestTemp = newLowestTemp;
	}
	
	public int getWarranty()
	{
		return this.warranty;
	}
	public String getBrand()
	{
		return this.brand;
	}
	public int getLowestTemp()
	{
		return this.lowestTemp;
	}
	public int getCurrentTemp()
	{
		return this.currentTemp;
	}
	
	public void refrigerate()
	{
		System.out.println("Please give your refrigerator 24 hours to reach a stable temperature upon initial startup");
	}	
	public void freeze()
	{
		if (lowestTemp <= 0)
		{
			System.out.println("Your food product will freeze in approximately 2 hours");
		}
		else
		{
			System.out.println("Please lower your freezer's temperature to 0 degree or lower for optimal effectiveness");
		}
	}	
	
	private int validate(int tempAdded)
	{
		if (tempAdded >= 0)
		{
			return tempAdded;
		}
		else
		{
			tempAdded = tempAdded*-1;
			return tempAdded;
		}
	}
	
	public void printTemp(int tempAdded)
	{
		validate(tempAdded);
		this.currentTemp = this.currentTemp + tempAdded;
		System.out.println("Your current temperature is: " + currentTemp);
	}
}

import java.util.Scanner;

public class ApplianceStore
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Refrigerator[] appliances = new Refrigerator[4];
		
		for (int i = 0; i < appliances.length; i++)
		{		
			System.out.println("Please enter the brand of your refrigerator: ");
			String newBrand = scan.next();
			
			System.out.println("Please enter the number of year your warranty will last: ");
			int newWarranty = scan.nextInt();
			
			System.out.println("Please enter the lowest temperature(Celsius) your refrigerator can achieve: ");
			int newLowestTemp = scan.nextInt();
			
			appliances[i] = new Refrigerator(newWarranty, newBrand, newLowestTemp, 3); //3 is the average fridge temp	
		}
		
		System.out.println("Warranty: " + appliances[3].getWarranty());
		System.out.println("Brand: " + appliances[3].getBrand());
		System.out.println("Lowest Temperature: " + appliances[3].getLowestTemp());
		
		System.out.println("Please enter new information about the last refrigerator");
		int newWarranty = scan.nextInt();
		appliances[3].setWarranty(newWarranty);
		
		String newBrand = scan.next(); //idk how a refrigerator magically changes what brand it comes from
		appliances[3].setBrand(newBrand);
		
		int newLowestTemp = scan.nextInt();
		appliances[3].setLowestTemp(newLowestTemp);
		
		System.out.println("Warranty: " + appliances[3].getWarranty());
		System.out.println("Brand: " + appliances[3].getBrand());
		System.out.println("Lowest Temperature: " + appliances[3].getLowestTemp());

		appliances[1].printTemp(17);
	}
}